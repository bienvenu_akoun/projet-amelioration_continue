# Utilise l'image Maven avec OpenJDK 11 pour la compilation et le test
FROM maven:3.8.1-openjdk-11 AS build

# Définit le répertoire de travail dans le conteneur
WORKDIR /app

# Copie du code source Maven dans le conteneur
COPY . /app

# Exécute les commandes Maven pour le build et les tests
RUN cd junit5-maven && mvn clean install

# Utilise l'image Docker pour le déploiement
FROM docker:24.0.5 AS deploy

# Définit le répertoire de travail dans le conteneur
WORKDIR /deploy

# Copie des artefacts de build depuis l'image précédente
COPY --from=build /app/junit5-maven/target /deploy

# Exécute la construction de l'image Docker
# RUN docker build -t my-docker-image .

