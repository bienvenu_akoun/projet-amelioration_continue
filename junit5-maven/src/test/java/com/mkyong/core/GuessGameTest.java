/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

/**
 *
 * @author romai
 */
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Disabled;

public class GuessGameTest {

    @Test
    public void testInvalidGuess() {
        GuessGame game = new GuessGame();
        assertEquals("Your guess is invalid", game.determineGuess(0, 50, 1));
        assertEquals("Your guess is invalid", game.determineGuess(101, 50, 1));
    }

    @Test
    public void testCorrectGuess() {
        GuessGame game = new GuessGame();
        assertEquals("Correct!\nTotal Guesses: 1", game.determineGuess(50, 50, 1));
    }

    @Test
    public void testHighGuess() {
        GuessGame game = new GuessGame();
        assertEquals("Your guess is too high, try again.\nTry Number: 1", game.determineGuess(75, 50, 1));
    }

    @Test
    public void testLowGuess() {
        GuessGame game = new GuessGame();
        assertEquals("Your guess is too low, try again.\nTry Number: 1", game.determineGuess(25, 50, 1));
    }

    @Test
    @Disabled
    public void testIncorrectGuess() {
        GuessGame game = new GuessGame();
        assertEquals("Your guess is incorrect\nTry Number: 1", game.determineGuess(30, 50, 1));
    }
}
