/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Disabled;

/**
 *
 * @author romai
 */
public class TestCalculatrice {
    
    
/* ----------------------------------------------------------------------------- */    
/*
    // Test Addition
    @DisplayName("Test Calculatrice.addition()")
    @Test
    void testAddition() {
        assertEquals(4, Calculatrice.addition(2,2));
        assertEquals(-2, Calculatrice.addition(-1,-1));
        assertEquals(0, Calculatrice.addition(-1,1));
    } 
    
    // Test Soustraction
    @DisplayName("Test Calculatrice.soustraction()")
    @Test
    void testSoustraction() {
        assertEquals(6, Calculatrice.soustraction(8,2));
        assertEquals(5, Calculatrice.soustraction(10,5));
        assertEquals(-5, Calculatrice.soustraction(-10,-5));
    }  
    
    // Test Multiplication
    @DisplayName("Test Calculatrice.multiplication()")
    @Test
    void testMultiplication() {
        assertEquals(4, Calculatrice.multiplication(2,2));
        assertEquals(16, Calculatrice.multiplication(8,2));
        assertEquals(45, Calculatrice.multiplication(9,5));
    } 
    
    
    // Test Division
    @DisplayName("Test Calculatrice.division()")
    @Test
    void testDivisionNormale() {
        assertEquals(9, Calculatrice.division(27,3));
        assertEquals(10, Calculatrice.division(40,4));
    } 
    
    @DisplayName("Test Calculatrice.division()")
    @Test
    void testDivisionIncorrecte() {
        Assertions.assertNull(Calculatrice.division(27,0));
    } 

*/

/* ----------------------------------------------------------------------------- */  
  
 @DisplayName("Test Calculatrice.addition()")
 @Test
    public void testAddition() {
        assertEquals(5, Calculatrice.addition(2, 3));
    }

    @DisplayName("Test Calculatrice.soustraction()")
    @Test
    public void testSoustraction() {
        Assertions.assertNotEquals(2, Calculatrice.soustraction(5, 2));
    }

    @DisplayName("Test Calculatrice.multiplication()")
    @Test
    public void testMultiplication() {
        assertEquals(15, Calculatrice.multiplication(3, 5));
    }

    @DisplayName("Test Calculatrice.division()")
    @Test
    public void testDivision() {
        Assertions.assertNull(Calculatrice.division(5, 0));
        assertEquals(3, Calculatrice.division(9, 3));
    }

    @DisplayName("Test Calculatrice.calcul()")
    @Test
    @Disabled
    public void testCalcul() {
        Assertions.assertEquals(10, Calculatrice.calcul(6, 4, "+"));
        Assertions.assertNotEquals(5, Calculatrice.calcul(8, 3, "/"));
        Assertions.assertNotNull(Calculatrice.calcul(2, 3, "*"));
        Assertions.assertTrue(Calculatrice.calcul(6, 3, "/") instanceof Integer);
        Assertions.assertFalse(Calculatrice.calcul(4, 2, "+") instanceof Integer);
    }

    @DisplayName("Test Calculatrice.calcul()")
    @Test
    @Disabled
    public void testUnsupportedOperator() {
        try {
            Calculatrice.calcul(4, 2, "%");
            fail("Une exception aurait dû être lancée pour un opérateur non pris en charge.");
        } catch (IllegalArgumentException e) {
            assertEquals("Opérateur non pris en charge.", e.getMessage());
        }
    }
    
    /*
    @DisplayName("Test Calculatrice.addition()")
    @Test
    public void testAddition() {
        assertThat(Calculatrice.addition(2,3)).isEqualTo(5);
    }

    @DisplayName("Test Calculatrice.multiplication()")
    @Test
    public void testMultiplication() {
        assertThat(Calculatrice.multiplication(3,5)).isEqualTo(15);
    }
    
    */
    
}
