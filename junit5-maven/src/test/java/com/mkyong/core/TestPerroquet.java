/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 *//*
package com.mkyong.core;

import java.time.Duration;
import java.time.Instant;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 *
 * @author romai
 */
/*
public class TestPerroquet {
    

    private static Instant startTime;
    private static Instant endTime;

    @BeforeAll
    static void beforeAllTests() {
        System.out.println("Début de tous les tests (beforeAll).");
        startTime = Instant.now();
    }

    
    @BeforeEach
    void beforeEachTest() {
        System.out.println("Début du test perroquetParlant(beforeEach).");
    }
    
    @DisplayName("Test Perroquet.perroquetParlant()")
    @Test
    void testPerroquetParlant() {
        assertEquals("Hello", Perroquet.perroquetParlant("Hello"));
        assertEquals("Bonjour", Perroquet.perroquetParlant("Bonjour"));
    }

    @AfterEach
    void afterEachTest() {
        System.out.println("Fin du test perroquetParlant(afterEach).");
    }



    @Disabled // ou @Ignore dans les versions antérieures
    @DisplayName("Test Perroquet.perroquetBonjour()")
    @Test
    void testPerroquetBonjour() {
        assertEquals("Bonjour Alice", Perroquet.perroquetBonjour("Alice"));
        assertEquals("Bonjour Bob", Perroquet.perroquetBonjour("Bob"));
    }
    
    
    @AfterAll
    static void afterAllTests() {
        endTime = Instant.now();
        Duration duration = Duration.between(startTime, endTime);
        System.out.println("Fin de tous les tests (afterAll). Temps total : " + duration.toMillis() + " ms");
    }


    
    @ParameterizedTest
    @CsvSource({"Hello, Hello", "Bonjour, Bonjour", "Hi, Hi"})
    @DisplayName("Test Perroquet.perroquetParlant()")
    void testPerroquetParlant(String input, String expectedOutput) {
        assertEquals(expectedOutput, Perroquet.perroquetParlant(input));
    }

    @ParameterizedTest
    @CsvSource({"Alice, Bonjour Alice", "Bob, Bonjour Bob", "Charlie, Bonjour Charlie"})
    @DisplayName("Test Perroquet.perroquetBonjour()")
    void testPerroquetBonjour(String input, String expectedOutput) {
        assertEquals(expectedOutput, Perroquet.perroquetBonjour(input));
    }
    
    
    
    
}
*/