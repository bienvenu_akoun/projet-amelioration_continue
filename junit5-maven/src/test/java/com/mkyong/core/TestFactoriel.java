/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 *
 * @author romai
 */
public class TestFactoriel {
    
    
    // Test factoriel itératif
    @DisplayName("Test Factoriel.factorielIteratif()")
    @Test
    void testFactorielIteratif() {
        assertEquals(120, Factoriel.factorielIteratif(5));
        assertEquals(1, Factoriel.factorielIteratif(0));
        assertEquals(1, Factoriel.factorielIteratif(1));
    }
    
    
     // Test factoriel récursif
    @DisplayName("Test Factoriel.factorielRecursif()")
    @Test
    void testFactorielRecursif() {
        assertEquals(120, Factoriel.factorielRecursif(5));
        assertEquals(1, Factoriel.factorielRecursif(0));
        assertEquals(1, Factoriel.factorielRecursif(1));
    }
   
    
    
    
    
    
    
    
    
}
