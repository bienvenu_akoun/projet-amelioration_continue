package com.mkyong.core;


import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author romai
 */
public class TestExercice6 {
    
    
    @Timeout(30)
    @Test
    void testFunctionWithTimeout() {
        int[] delays = {10};

        for (int delay : delays) {
            boolean result = Exercice6.waitForTime(delay);
            assertTrue(result, "Function should return true after waiting for " + delay + " seconds.");
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}
