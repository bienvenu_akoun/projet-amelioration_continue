/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

/**
 *
 * @author romai
 */
public class Factoriel {
    
    public static long factorielRecursif(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorielRecursif(n - 1);
        }
    }

    
    public static long factorielIteratif(int n) {
        long resultat = 1;
        for (int i = 1; i <= n; i++) {
            resultat *= i;
        }
        return resultat;
    }
    
    
       
    
}
