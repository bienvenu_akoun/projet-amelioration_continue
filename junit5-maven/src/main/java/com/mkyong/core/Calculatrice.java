/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

/**
 *
 * @author romai
 */
public class Calculatrice {
    
    // Addition
    public static int addition(int a, int b) { 
       int leRes =  a+b ;
       return leRes ;
    }
       
    // Soustraction
    public static int soustraction(int a, int b) { 
       int leRes =  a-b ;
       return leRes ;
    }
    
    // Multiplication
    public static int multiplication(int a, int b) { 
       int leRes =  a*b ;
       return leRes ;
    }
    
    // Division
    public static Integer division(int a, int b) {
        
        int leRes = 0 ;       
        if(b==0){
            return null;
        }
        else {
           leRes =  a/b ;
        }
       return leRes ;
    }
    
    // Fonction calcul
 public static Integer calcul(Integer a, Integer b, String operateur) {
    
 switch (operateur) {
            case "+":
                return addition(a, b);
            case "-":
                return soustraction(a, b);
            case "*":
                return multiplication(a, b);
            case "/":
                return division(a, b);
            default:
                    System.out.println("Opérateur non pris en charge.");
                    return 0;
        }   
    
    }
    
    
    
    
    
}
